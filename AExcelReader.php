<?
namespace a161;
class AExcelReader
{
	private $result;
	private $file;
	private $type;
	private $table;
	private $fileName;
	
	public function __construct($file, $orig = false)
	{
		$this->fileName = ($orig === false)? $file : $orig;
		$this->file = $_SERVER['DOCUMENT_ROOT'] . $file;
		if(empty(trim($this->file)))
			throw new \Exception('Файл не загружен на сервер');
		if(!file_exists($this->file))
			throw new \Exception('Файл '.$file.' не существует');
		$tmp = explode('.', $file);
		$f = array_pop($tmp);			
			if(strtolower($f) == 'csv')
				$this->type = 0;
			elseif(strtolower($f) == 'xlsx')
				$this->type = 1;
			else
				throw new \Exception('Недопустимый тип файла: '.$file.' ('.$this->fileName.')');
	}
	
	private function getCSV()
	{
		$table = file($this->file);
		foreach($table as $row) 
		{
			$row = iconv('windows-1251', 'UTF-8', $row);
			$this->table[] = explode(';', $row);
		}
		return $this->table;
	}
	
	private function getXLSX()
	{
		$strings = [];
		$data = [];
		$i = 0;
		$zip = zip_open($this->file);
		if(is_resource($zip)) 
		{
			while ($zip_entry = zip_read($zip)) 
			{
				if(zip_entry_name($zip_entry) == 'xl/sharedStrings.xml')
					if(zip_entry_open($zip, $zip_entry, "r"))
					{
						$tmp = [];
						$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						$xml = simplexml_load_string($buf);
						$json = json_encode($xml);
						$tmp = json_decode($json, TRUE);
						foreach($tmp['si']  as $v)
						{
							if(isset($v['t']))
								$strings[] = $v['t'];
							elseif(isset($v['r']) && is_array($v['r']))
							{
								$t = '';
								foreach($v['r'] as $vv)
									$t .= $vv['t'];
								$strings[] = $t;
							}
						}
					}
				if(strpos(zip_entry_name($zip_entry), 'xl/worksheets/sheet') !== false)
					if(zip_entry_open($zip, $zip_entry, "r"))
					{
						$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						$xml = simplexml_load_string($buf);
						$json = json_encode($xml);
						$data[$i] = json_decode($json, TRUE);
						$i++;
					}
				zip_entry_close($zip_entry);
			}
			//lg($data,'81');
			$j = 0;
			foreach($data as $v)
			{
				foreach($v['sheetData']['row'] as $vv)
				{
					$i = 0;
					foreach($vv['c'] as $vvv)
					{
						if(isset($vvv['@attributes']['t']))
						{
							if($vvv['@attributes']['t'] == 's')
								$this->table[$j][$i++] = $strings[$vvv['v']];
						}
						elseif(isset($vvv['v']))
							$this->table[$j][$i++] = $vvv['v'];
						else
							$this->table[$j][$i++] = '';
					}
					$j++;
				}
			}
		}
		else
			throw new \Exception('Файл '.$this->file.' ('.$this->fileName.') не является файлом .xlsx, либо поврежден.');
		return $this->table;
	}
	
	public function getData()
	{
		return ($this->type === 0)? $this->getCSV() : $this->getXLSX();
	}
}
