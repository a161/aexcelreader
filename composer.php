{
  "name": "a161/aexcelreader",
  "description": "excel (xlsx) reading",
  "homepage": "https://athina.club/",
  "keywords": [
    "excel reading"
  ],
  "license": "MIT",
  "require": {
    "php": ">=5.4.0",
    "a161/aexcelreader": "dev-master"
  },
  "repositories": [
    {
      "type": "git",
      "url": "https://bitbucket.org/a161/aexcelreader"
    }
  ]
}